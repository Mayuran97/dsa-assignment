package palindrome;

import java.util.Scanner;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class PalindromeStackQueue {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		String line;

		do {
			System.out.print("Enter the word : ");
			line = scan.nextLine();
			line = line.toLowerCase();
			line = line.replaceAll(" ", "");
			line = line.replaceAll("\\W", "");

			if (is_palindrome(line))
				System.out.println("This is Palindrome");
			else
				System.out.println("This is not a palindrome");
		} while (line.length() != 0);
	}

	public static boolean is_palindrome(String input) {
		Queue<Character> q = new LinkedList<Character>();
		Stack<Character> s = new Stack<Character>();
		Character letter;
		int count = 0;

		for (int i = 0; i < input.length(); i++) {
			letter = input.charAt(i);
			if (Character.isLetter(letter)) {
				q.add(letter);
				s.push(letter);
			}
		}
		while (!q.isEmpty()) {
			if (q.remove() != s.pop())
				count++;
		}
		return (count == 0);
	}
}
