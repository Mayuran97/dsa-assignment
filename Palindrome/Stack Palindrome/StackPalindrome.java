package palindrome;

import java.util.Scanner;
import java.util.Stack;

public class StackPalindrome {

	public static void main(String[] args) {
		while (true) {
			Scanner scan = new Scanner(System.in);

			System.out.print("Enter the word : ");
			String read = scan.nextLine();
			String inputWord = read.toUpperCase();
			
		//	String inputWord = scan.nextLine();
			Stack<Character> stack = new Stack<Character>();

			for (int i = 0; i < inputWord.length(); i++) {
				stack.push(inputWord.charAt(i));

			}
			String reverseString = "";

			while (!stack.isEmpty()) {
				reverseString = reverseString + stack.pop();

			}

			if (inputWord.equals(reverseString))
				System.out.println("!!* This is a palindrome *!!");
			else
				System.out.println("!!* This is not a palindrome *!!");
			System.out.println("-------------------------------------");
		}
	}
}
