package kotSystem;

import java.util.Scanner;

import kotSystem.Waiter;

public class MainMenu {
	static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {
		Welcome();
		getUser();
	}

	public static void Welcome() {

		System.out.println("|--------------------------------------------------|");
		System.out.println("|**************************************************|");
		System.out.println("|_-_-_-_-_-_-_**** Jaffna Kitchen ****_-_-_-_-_-_-_|");
		System.out.println("|**************************************************|");
		System.out.println("|--------------------------------------------------|\n");

		System.out.println("                   *** Welcome ***");

	}

	public static void getUser() {

		System.out.println();
		System.out.println("---------------------------------------------------------------");
		System.out.println("Hi Sir/Mam  User option ? ");
		System.out.println("\t1 - Waiter \n\t0 - Exit");
		System.out.println("----------------------------------------------------------------");
		String type = scan.next();

		switch (type) {
		case "1":
			Waiter waiter = new Waiter();
			waiter.start();
			break;
		
		case "0":
			System.out.println("*** Thank you! Welcome ***");
			System.exit(0);
			break;
		default:
			System.err.println("-------!!!! Please Select Correct Option !!!!-------");
			getUser();
			break;
		}

		// Table table = new Table();

		// Table.tableList();
		// int read = scan.nextInt();

	}
}
