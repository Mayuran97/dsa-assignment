package palindrome;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class QueuePalindrome {

	public static void main(String[] args) {
		while(true) {
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the word : ");
		String read = scan.nextLine();
		String inputString = read.toUpperCase();
		
		Queue<Character> queue = new LinkedList<Character>();

		for (int i = inputString.length() - 1; i >= 0; i--) {
			queue.add(inputString.charAt(i));
		}

		String reverseString = "";

		while (!queue.isEmpty()) {
			reverseString = reverseString + queue.remove();
		}
		if (inputString.equals(reverseString))
			System.out.println("*** This is palindrome ***");
		else
			System.out.println("### This is not a palindrome ###");
		System.out.println("-----------------------------------");
		}
	}
}
